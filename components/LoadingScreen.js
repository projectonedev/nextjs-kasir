import { Box, Button, Container, Typography } from '@mui/material';

export const LoadingScreen = () => (
  <Box
    component="main"
    sx={{
      backgroundColor: 'black',
      alignItems: 'center',
      display: 'flex',
      flexGrow: 1,
      minHeight: '100%'
    }}
  >
    <Container maxWidth="md">
      <Box
        sx={{
          alignItems: 'center',
          display: 'flex',
          flexDirection: 'column'
        }}
      >
        <Typography
          align="center"
          color="white"
          variant="h4"
        >
          Loading...
        </Typography>
      </Box>
    </Container>
  </Box>
)