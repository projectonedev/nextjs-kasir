import { useState, useEffect } from "react";
import NextLink from 'next/link';
import { useRouter } from 'next/router';
import { Box, Button, ListItem, Collapse } from '@mui/material';

import ChevronRightIcon from '@mui/icons-material/ChevronRight';
import ExpandMore from '@mui/icons-material/ExpandMore';

const NavNestedItem = (props) => {
  const { icon, title, subItems, active, ...others } = props;
  const router = useRouter();

  const [open, setOpen] = useState(active);

  const handleClick = () => {
    setOpen(!open);
  };

  return (
    <>
      <ListItem
        disableGutters
        sx={{
          display: 'flex',
          mb: 0.5,
          py: 0,
          px: 2
        }}
        {...others}
      >
        <Button
          onClick={handleClick}
          component="a"
          startIcon={icon}
          endIcon={open ? <ExpandMore /> : <ChevronRightIcon />}
          disableRipple
          sx={{
            backgroundColor: active && 'rgba(255,255,255, 0.08)',
            borderRadius: 1,
            color: active ? 'secondary.main' : 'neutral.300',
            fontWeight: active && 'fontWeightBold',
            justifyContent: 'flex-start',
            px: 3,
            textAlign: 'left',
            textTransform: 'none',
            width: '100%',
            '& .MuiButton-startIcon': {
              color: active ? 'secondary.main' : 'neutral.400'
            },
            '& .MuiButton-endIcon': {
              color: 'neutral.300'
            },
            '&:hover': {
              backgroundColor: 'rgba(255,255,255, 0.08)'
            }
          }}
        >
          <Box sx={{ flexGrow: 1 }}>
            {title}
          </Box>
        </Button>
      </ListItem>
      <Collapse in={open} timeout="auto" unmountOnExit>
        <Box sx={{ flexGrow: 1, px: 2 }}>
          {subItems.map((item) => (
            <NavItem
              key={item.title}
              icon={item.icon}
              href={item.href}
              title={item.title}
            />
          ))}
        </Box>
      </Collapse>
    </>
  )
}

export const NavItem = (props) => {
  const { href, icon, title, subItems, ...others } = props;
  const router = useRouter();
  const [open, setOpen] = useState(active);

  var active = href ? (router.pathname === href) : false;

  if (subItems) {
    subItems.map((item) => {
      const _active = item.href ? (router.pathname === item.href) : false;
      if (_active) active = _active;
    });
  }

  if (href) {
    return (
      <ListItem
        disableGutters
        sx={{
          display: 'flex',
          mb: 0.5,
          py: 0,
          px: 2
        }}
        {...others}
      >
        <NextLink
          href={href}
          passHref
        >
          <Button
            component="a"
            startIcon={icon}
            disableRipple
            sx={{
              backgroundColor: active && 'rgba(255,255,255, 0.08)',
              borderRadius: 1,
              color: active ? 'secondary.main' : 'neutral.300',
              fontWeight: active && 'fontWeightBold',
              justifyContent: 'flex-start',
              px: 3,
              textAlign: 'left',
              textTransform: 'none',
              width: '100%',
              '& .MuiButton-startIcon': {
                color: active ? 'secondary.main' : 'neutral.400'
              },
              '&:hover': {
                backgroundColor: 'rgba(255,255,255, 0.08)'
              }
            }}
          >
            <Box sx={{ flexGrow: 1 }}>
              {title}
            </Box>
          </Button>
        </NextLink>
      </ListItem>
    )
  } else {
    return (
      <NavNestedItem
        icon={icon}
        title={title}
        subItems={subItems}
        active={active}
      />
    )
  }
};