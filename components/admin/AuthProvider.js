import { useEffect } from "react";
import { useRouter } from "next/router";
import { useCookies } from "react-cookie";

import { Profile as getProfile } from "../../utils/HandlerApi";

import { publicRoutes, privateRoutes } from "../../routes";

export const AuthProvider = (props) => {
  const [cookie, setCookie, removeCookie] = useCookies(["token"]);
  const router = useRouter();

  useEffect(async () => {
    const res = await getProfile();

    if (
      !publicRoutes.includes(router.pathname) ||
      privateRoutes.includes(router.pathname)
    ) {
      if (!res.status) {
        removeCookie("token");
        window.location = "/admin/login";
      }
    }

    if (res.status) {
      const { setProfile } = props;
      if (setProfile !== undefined) {
        setProfile(res.admin);
      }
    }
  }, []);

  return (<></>);
}