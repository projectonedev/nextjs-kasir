import Head from "next/head";
import { useState } from "react";

import { styled } from "@mui/material/styles";
import { DashboardNavbar } from "../admin/DashboardNavbar";
import { DashboardSidebar } from "../admin/DashboardSidebar";

const DashboardLayoutRoot = styled("div")(({ theme }) => ({
  display: "flex",
  flex: "1 1 auto",
  maxWidth: "100%",
  paddingTop: 64,
  [theme.breakpoints.up("lg")]: {
    paddingLeft: 280,
  },
}));

export const AdminLayout = (props) => {
  const { children, titlePage, ...other } = props;
  const [isSidebarOpen, setSidebarOpen] = useState(true);

  return (
    <>
      <Head>
        <title>{titlePage} | Admin</title>
      </Head>
      <DashboardLayoutRoot>{children}</DashboardLayoutRoot>
      <DashboardNavbar onSidebarOpen={() => setSidebarOpen(true)} />
      <DashboardSidebar
        onClose={() => setSidebarOpen(false)}
        open={isSidebarOpen}
      />
    </>
  );
};
