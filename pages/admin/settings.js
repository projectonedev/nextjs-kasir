import { AdminLayout } from "../../components/layouts/AdminLayout";
import { AuthProvider } from "../../components/admin/AuthProvider";
import { useState } from "react";
import { Formik } from "formik";
import * as Yup from "yup";

import { ChangePassword, GetLoginsActive, DeleteLogin } from "../../utils/HandlerApi";
import { UAParser } from "../../utils/UAParser";

import {
  Accordion,
  AccordionDetails,
  AccordionSummary,
  Alert,
  Box,
  Button,
  Card,
  CardContent,
  CardHeader,
  Checkbox,
  Collapse,
  Container,
  Divider,
  FormControl,
  FormControlLabel,
  InputLabel,
  OutlinedInput,
  InputAdornment,
  Grid,
  FormHelperText,
  IconButton,
  TextField,
  Typography,
  CircularProgress,
  List,
  ListItem,
  ListItemText,
  ListItemAvatar,
  Avatar,
  Tooltip,
  Dialog,
  DialogTitle,
  DialogContent,
  DialogContentText,
  DialogActions
} from '@mui/material';

import CloseIcon from "@mui/icons-material/Close";
import Visibility from '@mui/icons-material/Visibility';
import VisibilityOff from '@mui/icons-material/VisibilityOff';
import ExpandMoreIcon from '@mui/icons-material/ExpandMore';
import DeleteIcon from '@mui/icons-material/Delete';
import FolderIcon from '@mui/icons-material/Folder';
import PhoneIphoneIcon from "@mui/icons-material/PhoneIphone";
import DeviceUnknownIcon from '@mui/icons-material/DeviceUnknown';
import ComputerIcon from '@mui/icons-material/Computer';
import TabletIcon from '@mui/icons-material/Tablet';

// import UAParser from "ua-parser-js";

function sleep(ms) {
  return new Promise(resolve => setTimeout(resolve, ms));
}

const SettingsNotifications = (props) => (
  <form {...props}>
    <Card>
      <CardHeader
        subheader="Manage the notifications"
        title="Notifications"
      />
      <Divider />
      <CardContent>
        <Grid
          container
          spacing={6}
          wrap="wrap"
        >
          <Grid
            item
            md={4}
            sm={6}
            sx={{
              display: 'flex',
              flexDirection: 'column'
            }}
            xs={12}
          >
            <Typography
              color="textPrimary"
              gutterBottom
              variant="h6"
            >
              Notifications
            </Typography>
            <FormControlLabel
              control={(
                <Checkbox
                  color="primary"
                  defaultChecked
                />
              )}
              label="Email"
            />
            <FormControlLabel
              control={(
                <Checkbox
                  color="primary"
                  defaultChecked
                />
              )}
              label="Push Notifications"
            />
            <FormControlLabel
              control={<Checkbox />}
              label="Text Messages"
            />
            <FormControlLabel
              control={(
                <Checkbox
                  color="primary"
                  defaultChecked
                />
              )}
              label="Phone calls"
            />
          </Grid>
          <Grid
            item
            md={4}
            sm={6}
            sx={{
              display: 'flex',
              flexDirection: 'column'
            }}
            xs={12}
          >
            <Typography
              color="textPrimary"
              gutterBottom
              variant="h6"
            >
              Messages
            </Typography>
            <FormControlLabel
              control={(
                <Checkbox
                  color="primary"
                  defaultChecked
                />
              )}
              label="Email"
            />
            <FormControlLabel
              control={<Checkbox />}
              label="Push Notifications"
            />
            <FormControlLabel
              control={(
                <Checkbox
                  color="primary"
                  defaultChecked
                />
              )}
              label="Phone calls"
            />
          </Grid>
        </Grid>
      </CardContent>
      <Divider />
      <Box
        sx={{
          display: 'flex',
          justifyContent: 'flex-end',
          p: 2
        }}
      >
        <Button
          color="primary"
          variant="contained"
        >
          Save
        </Button>
      </Box>
    </Card>
  </form>
);

const LoginCard = () => {
  const [expanded, setExpanded] = useState(false);
  const [loginActivities, setLoginActivities] = useState([]);

  const [dialogConfirmDeleteLogin, setDialogConfirmDeleteLogin] = useState({
    open: false,
    id: 0,
  })

  const handleConfirmDeleteLogin = (id) => {
    setDialogConfirmDeleteLogin({
      open: true,
      id: id,
    })
  }

  const handleCloseDeleteLogin = () => {
    setDialogConfirmDeleteLogin({
      open: false,
      id: 0,
    })
  }

  const handleDeleteLogin = async () => {
    handleCloseDeleteLogin();
    const res = await DeleteLogin(dialogConfirmDeleteLogin.id);
    if (res.status) {
      const res = await GetLoginsActive();
      if (res.status) {
        setLoginActivities(res.logins);
      }
    }
  }

  const handleChange = (panel) => async (event, isExpanded) => {
    setExpanded(isExpanded ? panel : false);

    if (!isExpanded) return;

    if (panel == 'login-activity') {
      setLoginActivities([]);

      const res = await GetLoginsActive();
      if (res.status) {
        setLoginActivities(res.logins);
      }
    }
  };

  const [allertMsgText, setAllertMsgText] = useState("");
  const [allertMsgType, setAllertMsgType] = useState("success");

  const setAlertMsg = (text, type) => {
    setAllertMsgText(text);
    setAllertMsgType(type);
  }

  const [showPassword, setShowPassword] = useState(false);

  const onSubmitHandler = async ({ password, new_password, repeat_new_password }) => {
    setAllertMsgText("");
    const res = await ChangePassword(password, new_password, repeat_new_password);
    if (res.status) {
      setAlertMsg(res.message, "success");
      setProfile(res.admin);
    } else {
      if (res.message) {
        setAlertMsg(res.message, "error");
      } else {
        setAlertMsg("Error", "error");
      }
      return false;
    }
  };

  const initialValues = {
    password: "",
    new_password: "",
    repeat_new_password: ""
  };

  const validationSchema = Yup.object({
    password: Yup.string()
      .min(6, "Kata Sandi Lama minimal 6 karakter")
      .required("Kata Sandi Lama wajib diisi"),
    new_password: Yup.string()
      .min(6, "Kata Sandi Baru minimal 6 karakter")
      .required("Kata Sandi Baru wajib diisi"),
    repeat_new_password: Yup.string()
      .oneOf(
        [Yup.ref("new_password")],
        "Konfirmasi Kata Sandi Baru dengan benar"
      )
      .required("Ulangi Kata Sandi Baru wajib diisi"),
  });

  return (
    <>
      <Card>
        <CardHeader
          title="Login"
        />
        <Divider />
        <Accordion disableGutters expanded={expanded === 'login-activity'} onChange={handleChange('login-activity')}>
          <AccordionSummary
            expandIcon={<ExpandMoreIcon />}
            aria-controls="panel2bh-content"
            id="panel2bh-header"
          >
            <Grid container sx={{ px: 1 }}>
              <Grid item xs={12} md={4}>
                <Typography sx={{ fontWeight: 'medium' }}>Tempat Anda Login</Typography>
              </Grid>
              <Grid item xs={12} md={8}>
                <Typography sx={{ color: 'text.secondary' }}>
                  Hapus jika Anda merasa tidak login ditempat lain
                </Typography>
              </Grid>
            </Grid>
          </AccordionSummary>
          <AccordionDetails>
            <Dialog
              open={dialogConfirmDeleteLogin.open}
              onClose={handleCloseDeleteLogin}
              aria-labelledby="alert-dialog-title"
              aria-describedby="alert-dialog-description"
            >
              <DialogTitle id="alert-dialog-title">
                {"Hapus aktifitas login?"}
              </DialogTitle>
              <DialogContent>
                <DialogContentText id="alert-dialog-description">
                  Ketika perangkan ini mengakses maka diperlukan login kembali.
                </DialogContentText>
              </DialogContent>
              <DialogActions>
                <Button onClick={handleCloseDeleteLogin}>Batal</Button>
                <Button onClick={handleDeleteLogin} autoFocus>
                  Hapus
                </Button>
              </DialogActions>
            </Dialog>
            <Box sx={{ p: 2 }}>
              {!(loginActivities.length > 0) ?
                (
                  <Box sx={{
                    width: 'max-content',
                    mx: 'auto',
                  }}>
                    <CircularProgress />
                  </Box>
                )
                :
                <List>
                  {loginActivities.map((loginActivity) => {
                    const ua = UAParser(loginActivity.device);

                    return (
                      <>
                        <ListItem key={loginActivity.id}
                          secondaryAction={
                            <Tooltip title="Hapus">
                              <IconButton onClick={() => handleConfirmDeleteLogin(loginActivity.id)} edge="end" aria-label="delete">
                                <DeleteIcon />
                              </IconButton>
                            </Tooltip>
                          }>
                          <ListItemAvatar>
                            <Avatar>
                              {{
                                "mobile": <PhoneIphoneIcon />,
                                "tablet": <TabletIcon />,
                                "pc": <ComputerIcon />,
                                undefined: <DeviceUnknownIcon />,
                                default: <DeviceUnknownIcon />
                              }[ua.type]}
                              {console.log(ua)}
                            </Avatar>
                          </ListItemAvatar>
                          <ListItemText
                            primary={ua.model ?? ua.ua}
                            secondary={ua.browser ?? "unknown browser"}
                          />
                        </ListItem>
                        <Divider variant="inset" component="li" />
                      </>
                    )
                  })
                  }
                </List>
              }
            </Box>
          </AccordionDetails>
        </Accordion>
        <Accordion disableGutters expanded={expanded === 'change-password'} onChange={handleChange('change-password')}>
          <AccordionSummary
            expandIcon={<ExpandMoreIcon />}
            aria-controls="panel1bh-content"
            id="panel1bh-header"
          >
            <Grid container sx={{ px: 1 }}>
              <Grid item xs={12} md={4}>
                <Typography sx={{ fontWeight: 'medium' }}>Ubah Kata Sandi</Typography>
              </Grid>
              <Grid item xs={12} md={8}>
                <Typography sx={{ color: 'text.secondary' }}>
                  Gunakan kata sandi yang kuat
                </Typography>
              </Grid>
            </Grid>
          </AccordionSummary>
          <AccordionDetails>
            <Formik
              initialValues={initialValues}
              onSubmit={onSubmitHandler}
              validationSchema={validationSchema}
            >
              {(formik) => (
                <form onSubmit={formik.handleSubmit}>

                  <Box sx={{ p: 2 }}>
                    <Box sx={{ width: "100%" }}>
                      <Collapse in={Boolean(allertMsgText != "")}>
                        <Alert
                          severity={allertMsgType}
                          action={
                            <IconButton
                              aria-label="close"
                              color="inherit"
                              size="small"
                              onClick={() => {
                                setAllertMsgText("");
                              }}
                            >
                              <CloseIcon fontSize="inherit" />
                            </IconButton>
                          }
                          sx={{ mb: 2 }}
                        >
                          {allertMsgText}
                        </Alert>
                      </Collapse>
                    </Box>
                    <Grid container spacing={3}>
                      <Grid item xs={12}>
                        <FormControl
                          error={Boolean(formik.touched.password && formik.errors.password)}
                          variant="outlined" fullWidth
                        >
                          <InputLabel htmlFor="outlined-adornment-password">Kata Sandi Lama</InputLabel>
                          <OutlinedInput
                            id="outlined-adornment-password"
                            type={showPassword ? 'text' : 'password'}
                            disabled={formik.isSubmitting}
                            label="Kata Sandi Lama"
                            name="password"
                            onBlur={formik.handleBlur}
                            onChange={formik.handleChange}
                            value={formik.values.password}
                            endAdornment={
                              <InputAdornment position="end">
                                <IconButton
                                  aria-label="toggle password visibility"
                                  onClick={() => setShowPassword(!showPassword)}
                                  onMouseDown={(e) => e.preventDefault()}
                                  edge="end"
                                >
                                  {showPassword ? <VisibilityOff /> : <Visibility />}
                                </IconButton>
                              </InputAdornment>
                            }
                          />
                          {(formik.touched.password && formik.errors.password) && <FormHelperText>{formik.errors.password}</FormHelperText>}
                        </FormControl>
                      </Grid>
                      <Grid item md={6} xs={12}>
                        <FormControl
                          error={Boolean(formik.touched.new_password && formik.errors.new_password)}
                          variant="outlined" fullWidth
                        >
                          <InputLabel htmlFor="outlined-adornment-new-password">Kata Sandi Baru</InputLabel>
                          <OutlinedInput
                            id="outlined-adornment-new-password"
                            type={showPassword ? 'text' : 'password'}
                            disabled={formik.isSubmitting}
                            label="Kata Sandi Baru"
                            name="new_password"
                            onBlur={formik.handleBlur}
                            onChange={formik.handleChange}
                            value={formik.values.new_password}
                            endAdornment={
                              <InputAdornment position="end">
                                <IconButton
                                  aria-label="toggle password visibility"
                                  onClick={() => setShowPassword(!showPassword)}
                                  onMouseDown={(e) => e.preventDefault()}
                                  edge="end"
                                >
                                  {showPassword ? <VisibilityOff /> : <Visibility />}
                                </IconButton>
                              </InputAdornment>
                            }
                          />
                          {(formik.touched.new_password && formik.errors.new_password) && <FormHelperText>{formik.errors.new_password}</FormHelperText>}
                        </FormControl>
                      </Grid>
                      <Grid item md={6} xs={12}>
                        <FormControl
                          error={Boolean(formik.touched.repeat_new_password && formik.errors.repeat_new_password)}
                          variant="outlined" fullWidth
                        >
                          <InputLabel htmlFor="outlined-adornment-repeat-new-password">Konfirmasi Kata Sandi Baru</InputLabel>
                          <OutlinedInput
                            id="outlined-adornment-repeat-new-password"
                            type={showPassword ? 'text' : 'password'}
                            disabled={formik.isSubmitting}
                            label="Konfirmasi Kata Sandi Baru"
                            name="repeat_new_password"
                            onBlur={formik.handleBlur}
                            onChange={formik.handleChange}
                            value={formik.values.repeat_new_password}
                            endAdornment={
                              <InputAdornment position="end">
                                <IconButton
                                  aria-label="toggle password visibility"
                                  onClick={() => setShowPassword(!showPassword)}
                                  onMouseDown={(e) => e.preventDefault()}
                                  edge="end"
                                >
                                  {showPassword ? <VisibilityOff /> : <Visibility />}
                                </IconButton>
                              </InputAdornment>
                            }
                          />
                          {(formik.touched.repeat_new_password && formik.errors.repeat_new_password) && <FormHelperText>{formik.errors.repeat_new_password}</FormHelperText>}
                        </FormControl>
                      </Grid>
                    </Grid>
                    <Box
                      sx={{
                        display: 'flex',
                        justifyContent: 'flex-end',
                        pt: 2
                      }}
                    >
                      <Button
                        disabled={formik.isSubmitting}
                        type="submit"
                        color="primary"
                        variant="contained"
                      >
                        Ubah Kata Sandi
                      </Button>
                    </Box>
                  </Box>
                </form>
              )}
            </Formik>

          </AccordionDetails>
        </Accordion>
      </Card>
    </>
  );
};

const Settings = () => {
  const [profile, setProfile] = useState({});

  return (
    <>
      <AuthProvider setProfile={setProfile} />
      <Box
        component="main"
        sx={{
          flexGrow: 1,
          py: 8
        }}
      >
        <Container maxWidth="lg">
          <Typography
            sx={{ mb: 3 }}
            variant="h4"
          >
            Settings
          </Typography>
          {/* <SettingsNotifications /> */}
          {/* <Box sx={{ pt: 3 }}> */}
          <LoginCard />
          {/* </Box> */}
        </Container>
      </Box>
    </>
  );
}


Settings.getLayout = (page) => (
  <AdminLayout titlePage="Settings">
    {page}
  </AdminLayout>
)

export default Settings;