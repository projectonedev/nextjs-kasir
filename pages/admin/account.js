import { AdminLayout } from "../../components/layouts/AdminLayout";
import { AuthProvider } from "../../components/admin/AuthProvider";
import { useState, useEffect } from "react";
import { useFormik, Formik } from "formik";
import * as Yup from "yup";

import {
  Alert,
  Avatar,
  Box,
  Button,
  Card,
  CardActions,
  CardContent,
  CardHeader,
  Collapse,
  Container,
  Divider,
  Grid,
  TextField,
  Typography,
} from "@mui/material";
import IconButton from "@mui/material/IconButton";
import CloseIcon from "@mui/icons-material/Close";

import { UpdateProfile } from "../../utils/HandlerApi";

const AccountProfile = (props) => {
  const { user, ...otherProps } = props;

  return (
    <Card {...otherProps}>
      <CardContent>
        <Box
          sx={{
            alignItems: "center",
            display: "flex",
            flexDirection: "column",
          }}
        >
          <Avatar
            src={user.avatar}
            sx={{
              height: 64,
              mb: 2,
              width: 64,
            }}
          />
          <Typography color="textPrimary" gutterBottom variant="h5">
            {user.name}
          </Typography>
          <Typography color="textSecondary" variant="body2">
            {user.email}
          </Typography>
          <Typography color="textSecondary" variant="body2">
            {user.timezone}
          </Typography>
        </Box>
      </CardContent>
      <Divider />
      <CardActions>
        <Button color="primary" fullWidth variant="text">
          Upload picture
        </Button>
      </CardActions>
    </Card>
  );
};

const AccountProfileDetails = (props) => {
  const { user, setProfile } = props;

  const [allertMsgText, setAllertMsgText] = useState("");
  const [allertMsgType, setAllertMsgType] = useState("success");

  const setAlertMsg = (text, type) => {
    setAllertMsgText(text);
    setAllertMsgType(type);
  }

  const onSubmitHandler = async ({ name, username, email }) => {
    setAllertMsgText("");
    const res = await UpdateProfile(name, username, email);
    if (res.status) {
      setAlertMsg(res.message, "success");
      setProfile(res.admin);
    } else {
      if (res.message) {
        setAlertMsg(res.message, "error");
      } else {
        setAlertMsg("Error", "error");
      }
      return false;
    }
  };

  const initialValues = {
    name: user.name || "",
    username: user.username || "",
    email: user.email || "",
  };

  const validationSchema = Yup.object({
    name: Yup.string()
      .matches(/^[a-zA-Z\-\s]+$/, "Masukkan nama dengan benar")
      .required("Nama wajib diisi"),
    username: Yup.string()
      .matches(/^[a-zA-Z0-9]+$/, "Nama Pengguna hanya boleh huruf dan angka")
      .min(3, "Nama Pengguna minimal 3 karakter")
      .max(30, "Nama Pengguna maksimal 30 karakter")
      .required("Username wajib diisi"),
    email: Yup.string()
      .email("Masukkan email dengan benar")
      .max(255)
      .required("Email wajib diisi"),
  });

  return (
    <Formik
      enableReinitialize={true}
      initialValues={initialValues}
      onSubmit={onSubmitHandler}
      validationSchema={validationSchema}
    >
      {(formik) => (
        <form onSubmit={formik.handleSubmit}>
          <Card>
            <CardHeader
              subheader="The information can be edited"
              title="Profile"
            />
            <Divider />
            <CardContent>
              <Box sx={{ width: "100%" }}>
                <Collapse in={Boolean(allertMsgText != "")}>
                  <Alert
                    severity={allertMsgType}
                    action={
                      <IconButton
                        aria-label="close"
                        color="inherit"
                        size="small"
                        onClick={() => {
                          setAllertMsgText("");
                        }}
                      >
                        <CloseIcon fontSize="inherit" />
                      </IconButton>
                    }
                    sx={{ mb: 2 }}
                  >
                    {allertMsgText}
                  </Alert>
                </Collapse>
              </Box>
              <TextField
                disabled={formik.isSubmitting}
                error={Boolean(formik.touched.name && formik.errors.name)}
                fullWidth
                helperText={formik.touched.name && formik.errors.name}
                label="Nama"
                name="name"
                margin="normal"
                onBlur={formik.handleBlur}
                onChange={formik.handleChange}
                value={formik.values.name}
                variant="outlined"
              />
              <TextField
                disabled={formik.isSubmitting}
                error={Boolean(formik.touched.username && formik.errors.username)}
                fullWidth
                helperText={formik.touched.username && formik.errors.username}
                label="Username"
                name="username"
                margin="normal"
                onBlur={formik.handleBlur}
                onChange={formik.handleChange}
                value={formik.values.username}
                variant="outlined"
              />
              <TextField
                disabled={formik.isSubmitting}
                error={Boolean(formik.touched.email && formik.errors.email)}
                fullWidth
                helperText={formik.touched.email && formik.errors.email}
                label="Email"
                name="email"
                margin="normal"
                onBlur={formik.handleBlur}
                onChange={formik.handleChange}
                value={formik.values.email}
                variant="outlined"
              />
            </CardContent>
            <Divider />
            <Box
              sx={{
                display: "flex",
                justifyContent: "flex-end",
                p: 2,
              }}
            >
              <Button
                disabled={formik.isSubmitting}
                type="submit"
                color="primary"
                variant="contained"
              >
                Simpan
              </Button>
            </Box>
          </Card>
        </form>
      )}
    </Formik>
  );
};

const Account = (props) => {
  const [profile, setProfile] = useState({});

  return (
    <>
      <AuthProvider setProfile={setProfile} />
      <Box
        component="main"
        sx={{
          flexGrow: 1,
          py: 8,
        }}
      >
        <Container maxWidth="lg">
          <Typography sx={{ mb: 3 }} variant="h4">
            Account
          </Typography>
          <Grid container spacing={3}>
            <Grid item lg={4} md={6} xs={12}>
              <AccountProfile user={profile} />
            </Grid>
            <Grid item lg={8} md={6} xs={12}>
              <AccountProfileDetails user={profile} setProfile={setProfile} />
            </Grid>
          </Grid>
        </Container>
      </Box>
    </>
  )
}

Account.getLayout = (page) => (
  <AdminLayout titlePage="Akun">
    {page}
  </AdminLayout>
)

export default Account;