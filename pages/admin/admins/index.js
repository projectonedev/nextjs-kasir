import { AdminLayout } from "../../../components/layouts/AdminLayout";
import { AuthProvider } from "../../../components/admin/AuthProvider";
import { useState, useEffect } from "react";
import NextLink from "next/link";
import { useRouter } from 'next/router'

import { GetAdminsAsync, UpdateStatusAdmin } from "../../../utils/HandlerApi"

import axios from "axios";

import {
  Box,
  Button,
  Container,
  Card,
  CardHeader,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableRow,
  Typography,
  Switch,
  Stack,
  Pagination,
  PaginationItem,
  Grid,
  IconButton,
  FormControl, InputLabel, OutlinedInput, InputAdornment,
  Select, MenuItem
} from "@mui/material"
import AddIcon from '@mui/icons-material/Add';
import ArrowBackIcon from '@mui/icons-material/ArrowBack';
import ArrowForwardIcon from '@mui/icons-material/ArrowForward';
import SearchIcon from '@mui/icons-material/Search';
import EditIcon from '@mui/icons-material/Edit';
import { SeverityPill } from "../../../components/SeverityPill";

import SimpleBar from 'simplebar-react';
import 'simplebar/dist/simplebar.min.css';

const TableAdmins = () => {
  const router = useRouter()
  const { page } = router.query
  const [admins, setAdmins] = useState([]);
  const [maxPages, setMaxPages] = useState(1);
  const [totalData, setTotalData] = useState(0);

  const [search, setSearch] = useState('');

  useEffect(() => {
    let unmounted = false;
    let source = axios.CancelToken.source();

    GetAdminsAsync(search, page ?? '').then((res) => {
      if (!unmounted) {
        if (res.status) {
          setAdmins(res.admins)
          setMaxPages(res.max_pages)
          setTotalData(res.total_data)
        }
      }
    })


    return function () {
      unmounted = true;
      source.cancel("Cancelling in cleanup");
    };
  }, [page, search])

  const switchStatusAdmin = async (e, id) => {
    const res = await UpdateStatusAdmin(id, e.target.checked);
    if (res.status) {
      const newList = admins.map((item) => {
        if (item.id === id) {
          const updatedItem = {
            ...item,
            status: res.admin.status,
          };

          return updatedItem;
        }

        return item;
      });

      setAdmins(newList)
    }
  }

  const searchAdmin = (event) => {
    const value = event.target.value
    setSearch(value)
    router.push({
      query: { search: value },
    });
  };

  const changePage = (event, value) => {
    router.push({
      query: { search: search, page: value },
    });
  };

  const editAdmin = (id) => {
    router.push(`/admin/admins/edit/${id}`)
  }

  return (
    <Card>
      <Grid container>
        <Grid item xs={12} sm={9} sx={{ p: 2 }}>
          <FormControl fullWidth>
            <OutlinedInput
              id="outlined-adornment-amount"
              placeholder="Search admin"
              onChange={searchAdmin}
              startAdornment={
                <InputAdornment position="start">
                  <SearchIcon />
                </InputAdornment>}
            />
          </FormControl>
        </Grid>
        <Grid item xs={12} sm={3} sx={{ p: 2 }}>
          <FormControl fullWidth>
            <InputLabel id="demo-simple-select-label">Age</InputLabel>
            <Select
              labelId="demo-simple-select-label"
              id="demo-simple-select"
              label="Age"
            >
              <MenuItem value={10}>Ten</MenuItem>
              <MenuItem value={20}>Twenty</MenuItem>
              <MenuItem value={30}>Thirty</MenuItem>
            </Select>
          </FormControl>
        </Grid>
      </Grid>
      <SimpleBar>
        <Table>
          <TableHead>
            <TableRow>
              <TableCell>
                Id
              </TableCell>
              <TableCell>
                Nama
              </TableCell>
              <TableCell>
                Username
              </TableCell>
              <TableCell>
                Email
              </TableCell>
              <TableCell>
                Status
              </TableCell>
              <TableCell>
                Aksi
              </TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {admins.map((admin) => (
              <TableRow
                hover
                key={admin.id}
              >
                <TableCell>
                  {admin.id}
                </TableCell>
                <TableCell>
                  {admin.name}
                </TableCell>
                <TableCell>
                  {admin.username}
                </TableCell>
                <TableCell>
                  {admin.email}
                </TableCell>
                <TableCell>
                  <Switch sx={{
                    '.MuiSwitch-thumb': {
                      color: 'rgb(107, 114, 128)'
                    },
                    '.Mui-checked .MuiSwitch-thumb': {
                      color: '#5048E5'
                    }
                  }}
                    onChange={(e) => switchStatusAdmin(e, admin.id)} color="primary" checked={admin.status} />
                </TableCell>
                <TableCell>
                  <IconButton
                    onClick={() => editAdmin(admin.id)}>
                   <EditIcon />
                  </IconButton>
                </TableCell>
              </TableRow>
            ))}
          </TableBody>
        </Table>
      </SimpleBar>
      <Grid container xs sx={{ p: 2 }}>
        <Grid item xs={6} sm={3} sx={{ display: 'flex', alignSelf: 'center' }}>
          <Typography sx={{ flexShrink: 0 }} variant="body2">Total Data: <b>{totalData}</b></Typography>
        </Grid>
        <Grid item xs={6} sm={3} sx={{ display: 'flex', alignSelf: 'center', justifyContent: { xs: 'flex-end', sm: 'flex-start' } }}>
          <Typography sx={{ flexShrink: 0 }} variant="body2">Total Data: <b>{totalData}</b></Typography>
        </Grid>
        <Grid item xs={12} sm={6} sx={{ display: 'flex', py: { xs: 1, sm: 0 }, justifyContent: { xs: 'center', sm: 'flex-end' } }}>
          <Stack spacing={2} sx={{ flexShrink: 0 }}>
            <Pagination
              onChange={changePage}
              count={maxPages}
              renderItem={(item) => (
                <PaginationItem
                  components={{ previous: ArrowBackIcon, next: ArrowForwardIcon }}
                  {...item}
                />
              )}
            />
          </Stack>
        </Grid>
      </Grid>
    </Card >
  )
}

const Admins = () => {
  const [profile, setProfile] = useState({});

  return (
    <>
      <AuthProvider setProfile={setProfile} />
      <Box
        component="main"
        sx={{
          flexGrow: 1,
          py: 8
        }}
      >
        <Container maxWidth="lg">
          <Box
            sx={{
              display: 'flex',
              justifyContent: 'space-between',
            }}
          >
            <Typography
              sx={{ mb: 3 }}
              variant="h4"
            >
              Admin
            </Typography>

            <Box>
              <NextLink
                href="/admin/admins/create"
                passHref
              >

                <Button
                  startIcon={<AddIcon />}
                  color="primary"
                  variant="contained"
                >
                  Tambah Admin
                </Button>
              </NextLink>
            </Box>
          </Box>

          <TableAdmins />
        </Container>
      </Box>
    </>
  )
}

Admins.getLayout = (page) => (
  <AdminLayout titlePage="Admin">
    {page}
  </AdminLayout>
)


export default Admins;