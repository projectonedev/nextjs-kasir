import { AdminLayout } from "../../../components/layouts/AdminLayout";
import { AuthProvider } from "../../../components/admin/AuthProvider";
import { useState, useEffect } from "react";
import { useRouter } from "next/router";

import { useFormik, Formik } from "formik";
import * as Yup from "yup";

import { GetAdmins } from "../../../utils/HandlerApi"
import Visibility from '@mui/icons-material/Visibility';
import VisibilityOff from '@mui/icons-material/VisibilityOff';

import { CreateAdmin } from "../../../utils/HandlerApi";

import {
  Box,
  Button,
  Container,
  Card,
  CardHeader,
  CardContent,
  Typography,
  Divider,
  Collapse,
  Alert,
  IconButton,
  TextField,
  Grid,
  FormControl,
  InputLabel,
  OutlinedInput,
  InputAdornment,
  FormHelperText
} from "@mui/material"
import CloseIcon from '@mui/icons-material/Close';

const CreateAdminCard = () => {
  const router = useRouter();

  const [showPassword, setShowPassword] = useState(false);

  const [allertMsgText, setAllertMsgText] = useState("");
  const [allertMsgType, setAllertMsgType] = useState("success");

  const setAlertMsg = (text, type) => {
    setAllertMsgText(text);
    setAllertMsgType(type);
  }

  const onSubmitHandler = async ({ name, username, email, password, repeat_password }) => {
    setAllertMsgText("");
    const res = await CreateAdmin(name, username, email, password, repeat_password);
    if (res.status) {
      setAlertMsg(res.message, "success");

      router.push("/admin/admins")
    } else {
      if (res.message) {
        setAlertMsg(res.message, "error");
      } else {
        setAlertMsg("Error", "error");
      }
    }
  };

  const initialValues = {
    name: "",
    username: "",
    email: "",
    password: "",
    repeat_password: ""
  };

  const validationSchema = Yup.object({
    name: Yup.string()
      .matches(/^[a-zA-Z\-\s]+$/, "Masukkan nama dengan benar")
      .required("Nama wajib diisi"),
    username: Yup.string()
      .matches(/^[a-zA-Z0-9]+$/, "Nama Pengguna hanya boleh huruf dan angka")
      .min(3, "Nama Pengguna minimal 3 karakter")
      .max(30, "Nama Pengguna maksimal 30 karakter")
      .required("Username wajib diisi"),
    email: Yup.string()
      .email("Masukkan email dengan benar")
      .max(255)
      .required("Email wajib diisi"),
    password: Yup.string()
      .min(6, "Kata Sandi minimal 6 karakter")
      .required("Kata Sandi wajib diisi"),
    repeat_password: Yup.string()
      .oneOf(
        [Yup.ref("password")],
        "Konfirmasi Kata Sandi dengan benar"
      )
      .required("Ulangi Kata Sandi wajib diisi"),

  });

  return (
    <Formik
      enableReinitialize={true}
      initialValues={initialValues}
      onSubmit={onSubmitHandler}
      validationSchema={validationSchema}
    >
      {(formik) => (
        <form onSubmit={formik.handleSubmit}>
          <Card>
            <CardHeader
              title="Informasi Admin"
            />
            <Divider />
            <CardContent>
              <Box sx={{ width: "100%" }}>
                <Collapse in={Boolean(allertMsgText != "")}>
                  <Alert
                    severity={allertMsgType}
                    action={
                      <IconButton
                        aria-label="close"
                        color="inherit"
                        size="small"
                        onClick={() => {
                          setAllertMsgText("");
                        }}
                      >
                        <CloseIcon fontSize="inherit" />
                      </IconButton>
                    }
                    sx={{ mb: 2 }}
                  >
                    {allertMsgText}
                  </Alert>
                </Collapse>
              </Box>
              <TextField
                disabled={formik.isSubmitting}
                error={Boolean(formik.touched.name && formik.errors.name)}
                fullWidth
                helperText={formik.touched.name && formik.errors.name}
                label="Nama"
                name="name"
                margin="normal"
                onBlur={formik.handleBlur}
                onChange={formik.handleChange}
                value={formik.values.name}
                variant="outlined"
              />
              <TextField
                disabled={formik.isSubmitting}
                error={Boolean(formik.touched.username && formik.errors.username)}
                fullWidth
                helperText={formik.touched.username && formik.errors.username}
                label="Username"
                name="username"
                margin="normal"
                onBlur={formik.handleBlur}
                onChange={formik.handleChange}
                value={formik.values.username}
                variant="outlined"
              />
              <TextField
                disabled={formik.isSubmitting}
                error={Boolean(formik.touched.email && formik.errors.email)}
                fullWidth
                helperText={formik.touched.email && formik.errors.email}
                label="Email"
                name="email"
                margin="normal"
                onBlur={formik.handleBlur}
                onChange={formik.handleChange}
                value={formik.values.email}
                variant="outlined"
              />
              <Grid container spacing={3} sx={{ pt: 2 }}>
                <Grid item md={6} xs={12}>
                  <FormControl
                    error={Boolean(formik.touched.password && formik.errors.password)}
                    variant="outlined" fullWidth
                  >
                    <InputLabel htmlFor="outlined-adornment-password">Kata Sandi</InputLabel>
                    <OutlinedInput
                      id="outlined-adornment-password"
                      type={showPassword ? 'text' : 'password'}
                      disabled={formik.isSubmitting}
                      label="Kata Sandi"
                      name="password"
                      onBlur={formik.handleBlur}
                      onChange={formik.handleChange}
                      value={formik.values.password}
                      endAdornment={
                        <InputAdornment position="end">
                          <IconButton
                            aria-label="toggle password visibility"
                            onClick={() => setShowPassword(!showPassword)}
                            onMouseDown={(e) => e.preventDefault()}
                            edge="end"
                          >
                            {showPassword ? <VisibilityOff /> : <Visibility />}
                          </IconButton>
                        </InputAdornment>
                      }
                    />
                    {(formik.touched.password && formik.errors.password) && <FormHelperText>{formik.errors.password}</FormHelperText>}
                  </FormControl>
                </Grid>
                <Grid item md={6} xs={12}>
                  <FormControl
                    error={Boolean(formik.touched.repeat_password && formik.errors.repeat_password)}
                    variant="outlined" fullWidth
                  >
                    <InputLabel htmlFor="outlined-adornment-repeat-password">Konfirmasi Kata Sandi</InputLabel>
                    <OutlinedInput
                      id="outlined-adornment-repeat-password"
                      type={showPassword ? 'text' : 'password'}
                      disabled={formik.isSubmitting}
                      label="Konfirmasi Kata Sandi"
                      name="repeat_password"
                      onBlur={formik.handleBlur}
                      onChange={formik.handleChange}
                      value={formik.values.repeat_password}
                      endAdornment={
                        <InputAdornment position="end">
                          <IconButton
                            aria-label="toggle password visibility"
                            onClick={() => setShowPassword(!showPassword)}
                            onMouseDown={(e) => e.preventDefault()}
                            edge="end"
                          >
                            {showPassword ? <VisibilityOff /> : <Visibility />}
                          </IconButton>
                        </InputAdornment>
                      }
                    />
                    {(formik.touched.repeat_password && formik.errors.repeat_password) && <FormHelperText>{formik.errors.repeat_password}</FormHelperText>}
                  </FormControl>
                </Grid>
              </Grid>

            </CardContent>
            <Divider />
            <Box
              sx={{
                display: "flex",
                justifyContent: "flex-end",
                p: 2,
              }}
            >
              <Button
                disabled={formik.isSubmitting}
                type="submit"
                color="primary"
                variant="contained"
              >
                Simpan
              </Button>
            </Box>
          </Card>
        </form>
      )}
    </Formik>
  )
}

const Create = () => {
  const [profile, setProfile] = useState({});

  return (
    <>
      <AuthProvider setProfile={setProfile} />
      <Box
        component="main"
        sx={{
          flexGrow: 1,
          py: 8
        }}
      >
        <Container maxWidth="lg">
          <Typography
            sx={{ mb: 3 }}
            variant="h4"
          >
            Tambah Admin
          </Typography>

          <CreateAdminCard />
        </Container>
      </Box>
    </>
  )
}

Create.getLayout = (page) => (
  <AdminLayout titlePage="Tambah Admin">
    {page}
  </AdminLayout>
)

export default Create;