import { useState } from "react";
import Head from "next/head";
import NextLink from "next/link";
import { useRouter } from "next/router";
import { useFormik } from "formik";
import * as Yup from "yup";
import {
  Alert,
  Box,
  Button,
  Collapse,
  Container,
  FormControl,
  FormHelperText,
  IconButton,
  InputAdornment,
  InputLabel,
  OutlinedInput,
  TextField,
  Typography,
} from "@mui/material";
import ArrowBackIcon from "@mui/icons-material/ArrowBack";
import Visibility from '@mui/icons-material/Visibility';
import VisibilityOff from '@mui/icons-material/VisibilityOff';
import CloseIcon from '@mui/icons-material/Close';
import { useCookies } from 'react-cookie'

import { Login as LoginHandler } from "../../utils/HandlerApi";

const Login = () => {
  const [cookie, setCookie] = useCookies(["token"]);

  const [allertMsgText, setAllertMsgText] = useState("");
  const [allertMsgType, setAllertMsgType] = useState("success");

  const [showPassword, setShowPassword] = useState(false);

  const setAlertMsg = (text, type) => {
    setAllertMsgText(text);
    setAllertMsgType(type);
  }

  const router = useRouter();
  const formik = useFormik({
    initialValues: {
      username: "",
      password: "",
    },
    validationSchema: Yup.object({
      username: Yup.string()
        .matches(/^[a-zA-Z0-9]+$/, "Nama Pengguna hanya boleh huruf dan angka")
        .min(3, "Nama Pengguna minimal 3 karakter")
        .required("Username wajib diisi"),
      password: Yup.string().max(255).required("Kata Sandi wajib diisi"),
    }),
    onSubmit: async ({ username, password }) => {
      setAllertMsgText("");
      const res = await LoginHandler(username, password);
      if (res.status) {
        setAlertMsg(res.message, "success");

        if (res.token !== undefined) {
          setCookie("token", res.token, {
            path: "/",
            maxAge: 3600 * 24 * 360, // Expires after 360 Day
            sameSite: true,
          });

          window.location = "/admin";
        }
      } else {
        if (res.message) {
          setAlertMsg(res.message, "error")
        } else {
          setAlertMsg("Error", "error")
        }
        return false;
      }
    },
  });

  return (
    <>
      <Head>
        <title>Masuk | Admin</title>
      </Head>
      <Box
        component="main"
        sx={{
          alignItems: "center",
          display: "flex",
          flexGrow: 1,
          minHeight: "100%",
        }}
      >
        <Container maxWidth="sm">
          <NextLink href="/admin" passHref>
            <Button
              component="a"
              startIcon={<ArrowBackIcon fontSize="small" />}
            >
              Dashboard
            </Button>
          </NextLink>
          <form onSubmit={formik.handleSubmit}>
            <Box sx={{ mt: 3 }}>
              <Typography color="textPrimary" variant="h4">
                Masuk Admin
              </Typography>
              <Typography color="textSecondary" gutterBottom variant="body2">
                Masuk untuk mengakses halaman admin
              </Typography>
            </Box>
            <Box sx={{ width: "100%" }}>
              <Collapse in={Boolean(allertMsgText != "")}>
                <Alert
                  severity={allertMsgType}
                  action={
                    <IconButton
                      aria-label="close"
                      color="inherit"
                      size="small"
                      onClick={() => {
                        setAllertMsgText("");
                      }}
                    >
                      <CloseIcon fontSize="inherit" />
                    </IconButton>
                  }
                  sx={{ mb: 2 }}
                >
                  {allertMsgText}
                </Alert>
              </Collapse>
            </Box>
            <TextField
              disabled={formik.isSubmitting}
              error={Boolean(formik.touched.username && formik.errors.username)}
              fullWidth
              helperText={formik.touched.username && formik.errors.username}
              label="Username"
              margin="normal"
              name="username"
              onBlur={formik.handleBlur}
              onChange={formik.handleChange}
              value={formik.values.username}
              variant="outlined"
            />
            <FormControl
              error={Boolean(formik.touched.password && formik.errors.password)}
              variant="outlined" fullWidth margin="normal"
            >
              <InputLabel htmlFor="outlined-adornment-password">Kata Sandi</InputLabel>
              <OutlinedInput
                id="outlined-adornment-password"
                type={showPassword ? 'text' : 'password'}
                disabled={formik.isSubmitting}
                label="Kata Sandi"
                name="password"
                onBlur={formik.handleBlur}
                onChange={formik.handleChange}
                value={formik.values.password}
                endAdornment={
                  <InputAdornment position="end">
                    <IconButton
                      aria-label="toggle password visibility"
                      onClick={() => setShowPassword(!showPassword)}
                      onMouseDown={(e) => e.preventDefault()}
                      edge="end"
                    >
                      {showPassword ? <VisibilityOff /> : <Visibility />}
                    </IconButton>
                  </InputAdornment>
                }
              />
              {(formik.touched.password && formik.errors.password) && <FormHelperText>{formik.errors.password}</FormHelperText>}
            </FormControl>
            <Box sx={{ py: 2 }}>
              <Button
                color="primary"
                disabled={formik.isSubmitting}
                fullWidth
                size="large"
                type="submit"
                variant="contained"
              >
                Masuk
              </Button>
            </Box>
          </form>
        </Container>
      </Box>
    </>
  );
};

export default Login;