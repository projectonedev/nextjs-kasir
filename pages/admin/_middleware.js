import { NextFetchEvent, NextRequest, NextResponse } from "next/server";
import { restrictedRoutes, privateRoutes, publicRoutes } from "../../routes"

export async function middleware(req, ev) {
  if (restrictedRoutes.includes(req.page.name)) {
    if (req.cookies.token) {
      return NextResponse.redirect("/admin")
    }
  } else if (privateRoutes.includes(req.page.name) || !publicRoutes.includes(req.page.name)) {
    if (!req.cookies.token) {
      return NextResponse.redirect("/admin/login")
    }
  }

  return NextResponse.next();
  // return new Response("Hello, world!");
}
