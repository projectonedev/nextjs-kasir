import { AdminLayout } from "../../components/layouts/AdminLayout";
import { AuthProvider } from "../../components/admin/AuthProvider";
import { useState } from "react";

const Dashboard = () => {
  const [profile, setProfile] = useState({});

  return (
    <>
      <AuthProvider setProfile={setProfile} />
      <div>Hello {profile.username}</div>
    </>
  );
}

Dashboard.getLayout = (page) => (
  <AdminLayout titlePage="Dashboard">
    {page}
  </AdminLayout>
)

export default Dashboard;