import Head from 'next/head';
import { useState, useEffect } from "react";

import { CacheProvider } from '@emotion/react';
import LocalizationProvider from '@mui/lab/LocalizationProvider';
import AdapterDateFns from '@mui/lab/AdapterDateFns';
import { CssBaseline } from '@mui/material';
import { ThemeProvider } from '@mui/material/styles';
import { createEmotionCache } from '../utils/CreateEmotionCache';
import { theme } from '../theme';
import axios from "axios";
import { useCookies } from 'react-cookie'

import { useRouter } from "next/router";

import { LoadingScreen } from '../components/LoadingScreen';

const clientSideEmotionCache = createEmotionCache();

const App = (props) => {
  const router = useRouter();
  const [loading, setLoading] = useState(false);

  const [cookie, setCookie] = useCookies(["token"]);

  axios.defaults.headers.common['Content-Type'] = 'application/json';
  if (cookie.token) {
    axios.defaults.headers.common['Authorization'] = `Bearer ${cookie.token}`;
  }

  const { Component, emotionCache = clientSideEmotionCache, pageProps } = props;

  const getLayout = Component.getLayout ?? ((page) => page);

  // useEffect(() => {
  //   router.events.on("routeChangeStart", () => setLoading(false));
  //   router.events.on("routeChangeError", () => setLoading(false));
  //   router.events.on("routeChangeComplete", () => setLoading(true));

  //   return () => {
  //     router.events.on("routeChangeStart", () => setLoading(false));
  //     router.events.on("routeChangeError", () => setLoading(fasle));
  //     router.events.on("routeChangeComplete", () => setLoading(true));
  //   }
  // }, [router.events]);

  return (
    <CacheProvider value={emotionCache}>
      <Head>
        <title>
          Material Kit Pro
        </title>
        <meta
          name="viewport"
          content="initial-scale=1, width=device-width"
        />
      </Head>
      <LocalizationProvider dateAdapter={AdapterDateFns}>
        <ThemeProvider theme={theme}>
          <CssBaseline />
          {loading ? (
            <LoadingScreen />
          ) :
            getLayout(<Component {...pageProps} />)
          }
        </ThemeProvider>
      </LocalizationProvider>
    </CacheProvider>
  );
};

export default App;