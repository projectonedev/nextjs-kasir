import axios from "axios";

const host = "http://192.168.100.5:5000";

const getRequest = async (path) => {
  try {
    const req = await axios.get(host + path);

    return req.data;
  } catch (e) {
    const response = e.response;
    if (response !== undefined && response.data !== undefined) {
      return response.data;
    } else {
      return {
        status: false,
        message: e.message,
      };
    }
  }
}

const postRequest = async (path, postData) => {
  try {
    const req = await axios.post(host + path, postData);

    return req.data;
  } catch (e) {
    const response = e.response;
    if (response !== undefined && response.data !== undefined) {
      return response.data;
    } else {
      return {
        status: false,
        message: e.message,
      };
    }
  }
}

export const Login = async (username, password) => {
  const req = await postRequest("/admin/auth/login", {
    username: username,
    password: password,
  });

  return req;
};

export const ChangePassword = async (password, new_password, repeat_new_password) => {
  const req = await postRequest("/admin/auth/change_password", {
    password: password,
    new_password: new_password,
    repeat_new_password: repeat_new_password
  });

  return req;
};

export const Logout = async () => {
  const req = await postRequest("/admin/auth/logout", {});

  return req;
};

export const Profile = async () => {
  const req = await getRequest("/admin/profile");

  return req;
}

export const UpdateProfile = async (name, username, email) => {
  const req = await postRequest("/admin/profile/update", {
    name: name,
    username: username,
    email: email,
  });

  return req;
};

export const GetAdminsAsync = (search, page) => {
  const req = getRequest(`/admin/admins/show?page=${page}&search=${search}`);

  return req;
}

export const GetAdmins = async (search, page) => {
  const req = await getRequest(`/admin/admins/show?page=${page}&search=${search}`);

  return req;
}

export const GetAdminById = async (id) => {
  const req = await postRequest(`/admin/admins/detail`, {
    id: id,
  });

  return req;
}

export const CreateAdmin = async (name, username, email, password, repeat_password) => {
  const req = await postRequest("/admin/admins/create", {
    name: name,
    username: username,
    email: email,
    password: password,
    repeat_password: repeat_password
  });

  return req;
};

export const UpdateStatusAdmin = async (id, status) => {
  const req = await postRequest("/admin/admins/update", {
    id: id,
    status: status,
  })

  return req;
}

export const UpdateAdmin = async (id, name, username, email, status) => {
  const req = await postRequest("/admin/admins/update", {
    id: id,
    name: name,
    username: username,
    email: email,
    status: status,
  })

  return req;
}

export const CheckUsername = async (username, currentUsername) => {
  const req = await postRequest("/admin/admins/check_username", {
    username: username,
    currentUsername: currentUsername,
  });

  return req;
}


export const GetLoginsActive = async () => {
  const req = await getRequest("/admin/auth/logins/active");

  return req;
}

export const DeleteLogin = async (id) => {
  const req = await postRequest("/admin/auth/logins/delete", {
    id: id,
  });

  return req;
};
