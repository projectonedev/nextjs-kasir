import { UAParser as uaParser } from "ua-parser-js";

export const UAParser = (ua) => {
  const parser = uaParser(ua);

  console.log(parser);

  // phone
  if (parser.device.type) {
    return {
      type: parser.device.type,
      model: parser.device.model,
      browser: parser.browser.name,  
      ua: parser.ua
    }
  } else if (parser.os.name) {
    return {
      type: 'pc',
      model: `${parser.os.name} ${parser.os.version}`,
      browser: parser.browser.name,
      ua: parser.ua
    }
  }

  return {
    type: undefined,
    model: undefined,
    browser: undefined,
    ua: parser.ua
  };
}