// private routes
export const privateRoutes = [
];
// public routes
export const publicRoutes = [
    // "/admin"
];

// restricted routes
export const restrictedRoutes = [
    "/admin/login"
];